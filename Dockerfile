FROM node:alpine AS builder

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci

COPY rollup.config.js ./
COPY src ./src
RUN npm run build

FROM nginx:1.16.0-alpine AS server

RUN apk add --no-cache gettext

COPY nginx-entrypoint.sh /
COPY --from=builder /app/dist /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 79

ENTRYPOINT [ "sh", "/nginx-entrypoint.sh" ]
